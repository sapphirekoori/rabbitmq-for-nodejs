let producerChannel = null;
let pendingMessages = [];

let init = (connection) => {
    connection.createConfirmChannel((error, channel) => {
        if (error) {
            console.log('Failed to create Producer channel.', error);
            throw error;
        }

        // set event for channel
        channel.on('error', (error) => {
            console.log('Error occurred in Producer channel.', error);
        });

        channel.on('close', () => {
            console.log('Producer channel closed.');
        });

        console.log('Successfully created Producer channel.');

        producerChannel = channel;
        sendPendingMessages();
    });
};

let send = (message, exchange, routingKey) => {
    if (!producerChannel) {
        // not yet initialized, add to pending
        addPendingMessage(message, exchange, routingKey);
        return;
    }

    producerChannel.checkExchange(exchange, (error, ok) => {
        if (error) {
            console.log(`Exchange ${ exchange } does not exist.`, error);
            addPendingMessage(message, exchange, routingKey);
            producerChannel.connection.close();
        }

        let content = null;

        if (message instanceof String) {
            content = Buffer.from(message);
        }
        else {
            content = Buffer.from(JSON.stringify(message));
        }

        producerChannel.publish(
            exchange,
            routingKey,
            content,
            { persistent: true },
            (error) => {
                if (error) {
                    console.log('Error occurred when sending message', error);
                    addPendingMessage(message, exchange, routingKey);
                    producerChannel.connection.close();
                }
                else {
                    console.log(`Message sent.\nContent: ${ content }`);
                }
            });
    });
};

let addPendingMessage = (message, exchange, routingKey) => {
    pendingMessages.push({
        body: message,
        exchange: exchange,
        routingKey: routingKey
    });
};

let sendPendingMessages = () => {
    while (pendingMessages.length > 0) {
        let message = pendingMessages.shift();
        send(message.body, message.exchange, message.routingKey);
    }
};

module.exports = {
    init,
    send
};