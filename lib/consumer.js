// Collection of BaseConsumer
let consumers = [];

let createChannel = (connection) => {
    return new Promise((resolve, reject) => {
        connection.createChannel((channelError, channel) => {
            if (channelError) {
                reject(channelError);
            }
            else {
                resolve(channel);
            }
        });
    });
};

module.exports = {
    init: function (connection) {
        for (let i = 0; i < consumers.length; i++) {
            let consumer = consumers[i];

            if (!consumer.queueName) {
                continue;
            }

            connection.createChannel((channelError, channel) => {
                if (channelError) {
                    console.log('Failed to create Consumer channel.', channelError);
                }
                else {
                    channel.on('error', (error) => {
                        console.log(`Error occurred in Consumer channel.\nConsumer: ${ consumer.constructor.name }, Queue: ${ consumer.queueName }`, error);
                    });
            
                    channel.on('close', () => {
                        console.log(`Consumer channel closed.\nConsumer: ${ consumer.constructor.name }, Queue: ${ consumer.queueName }`);
                    });

                    console.log(`Successfully created Consumer channel.\nConsumer: ${ consumer.constructor.name }, Queue: ${ consumer.queueName }`);

                    consumer.channel = channel;

                    channel.checkQueue(consumer.queueName, (queueError, queue) => {
                        if (queueError) {
                            console.log(`Check queue error.\nConsumer: ${ consumer.constructor.name }, Queue: ${ consumer.queueName }`, queueError);
                        }

                        channel.consume(consumer.queueName,
                            (data) => {
                                consumer.onMessageReceived(data);
                            },
                            function (consumeError) {
                                if (consumeError) {
                                    console.log(`Error ocurred when consuming message.\nConsumer: ${ consumer.constructor.name }, Queue: ${ consumer.queueName }`, consumeError);
                                }
                            }
                        );
                    });
                }
            });
        }
    },

    addConsumers: (consumerObject) => {
        if (Array.isArray(consumerObject)) {
            consumers = consumerObject;
        }
        else {
            consumers.push(consumerObject);
        }
    },
};