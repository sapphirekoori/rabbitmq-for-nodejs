var amqp = require('amqplib/callback_api');
var producer = require('./producer');
var consumer = require('./consumer');

let connectOptions = {
    protocol: 'amqp',
    hostname: null,
    port: 5672,
    username: null,
    password: null,
    frameMax: 0,
    heartbeat: 60,
    vhost: '/'
};

let connection = null;

const reconnectInterval = 30; // second

let connect = () => {
    amqp.connect(connectOptions, (error, conn) => {
        if (error) {
            console.log(`Failed to connect message queue server. Retry after ${ reconnectInterval } seconds.`, error);
            setTimeout(connect, reconnectInterval * 1000);
            return;
        }

        // set event for connection
        conn.on('error', (error) => {
            console.log('Error occurred on connection.', error);
        });

        conn.on('close', () => {
            console.log('Connection closed.');
        });
        
        console.log('Connected to message queue.');

        connection = conn;
        // should set consumer here as well ?
        producer.init(conn);
        consumer.init(conn);
    });
};

module.exports = {
    initConnection: (options) => {
        Object.assign(connectOptions, options);
    
        if (!connectOptions.hostname) {
            throw new Error('The host must be given to connect message queue.');
        }
    
        if (!connectOptions.username || !connectOptions.password) {
            throw new Error('The user and password must be given to connect message queue.');
        }

        connect();
    },

    addConsumer: (consumerObject) => {
        consumer.addConsumers(consumerObject);
    },

    send: (message, exchange, routingKey) => {
        producer.send(message, exchange, routingKey);
    }
};