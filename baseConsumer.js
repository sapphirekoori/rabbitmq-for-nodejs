class BaseConsumer {
    constructor() {
        if (new.target === BaseConsumer) {
            throw new TypeError('BaseConsumer is an abstract class which cannot be directly constructed.');
        }

        if (this.onMessageReceived === undefined) {
            throw new TypeError('Method onMessageReceived(message) must be defined.');
        }

        // queue name
        this.queueName = null;

        // amqp channel
        this.channel = null;
    }

    acknowledge(message) {
        this.channel.ack(message);
    }

    reject(message) {
        this.channel.nack(message, false, false);
    }

    close() {
        this.channel.close();
        this.channel = null;
    }
}

module.exports = BaseConsumer;