let connectionManager = require('./../lib/connectionManager');

let connectOptions = {
    protocol: 'amqp',
    hostname: 'localhost',
    port: 5672,
    username: 'admin',
    password: 'admin',
    vhost: '/'
};

connectionManager.initConnection(connectOptions);

let testMessage = {
    body: 'testing message'
};

connectionManager.send(testMessage, 'test-exchange', 'test.hello');