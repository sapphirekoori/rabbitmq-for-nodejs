let BaseConsumer = require('../baseConsumer');

class TestMessageConsumer extends BaseConsumer {
    constructor() {
        super();

        this.queueName = 'test-message';
        this.channel = null;
    }

    onMessageReceived(message) {
        try {
            console.log('message body: ' + message.content);
            this.acknowledge(message);
        }
        catch (error) {
            console.log(error);
            this.reject(message);
        }
    }
}

module.exports = TestMessageConsumer;