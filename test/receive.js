var connectionManager = require('./../lib/connectionManager');
var TestMessageConsumer = require('./testMessageConsumer');

let connectOptions = {
    protocol: 'amqp',
    hostname: 'localhost',
    port: 5672,
    username: 'admin',
    password: 'admin',
    vhost: '/'
};

let testConsumer = new TestMessageConsumer();

connectionManager.addConsumer(testConsumer);
connectionManager.initConnection(connectOptions);